var gulp            = require('gulp'),
    gulpif          = require('gulp-if'),
    yargs           = require('yargs'),
    autoprefixer    = require('gulp-autoprefixer'),
    imagemin        = require('gulp-imagemin'),
    watch           = require('gulp-watch'),
    twig            = require('gulp-twig'),
    runSequence     = require('run-sequence'),
    sourcemaps      = require('gulp-sourcemaps'),
    browserSync     = require('browser-sync').create(),
    concat          = require('gulp-concat'),
    csscomb         = require('gulp-csscomb'),
    cssnano         = require('gulp-cssnano'),
    rename          = require('gulp-rename'),
    uglify          = require('gulp-uglify'),
    del             = require('del');


// SETUP
// ---------------------------------------------------------------------

const PRODUCTION = !!(yargs.argv.production);
const DEVELOPMENT = !!(yargs.argv.development);


// Set source paths
var src = {
    path: 'src/',
    assetsDir: 'src/assets/',
    scripts: 'src/assets/js/**/*.js',
    images: 'src/assets/img/**/*',
    css: 'src/assets/css/**/*.css',
    fonts: 'src/assets/fonts/**/*',
    templates: 'src/templates/*.html',
    templatesAndIncludes: 'src/templates/**/*.html'
};

// Set distribution paths
var dist = {
    path: 'public_html',
    htmlDir: 'public_html/frontend/',
    assetsDir: 'public_html/assets',
    scriptsDir: 'public_html/assets/js',
    imagesDir: 'public_html/assets/img',
    fontsDir: 'public_html/assets/fonts',
    cssDir: 'public_html/assets/css',
    templates: 'public_html/*.html'
};

// Task runners
gulp.task('build', function(cb) {
    runSequence(['scripts', 'vendors'], 'copy-assets', 'images', 'templates', cb);
});

gulp.task('default', function(cb) {
    runSequence('build', 'server', 'watch', cb);
});

// TASKS
// ---------------------------------------------------------------------

//assets
gulp.task('assets-vendors', function() {
    //assets for plugins
    return gulp.src([

    ])
    .pipe(gulp.dest(dist.cssDir));
});

// Copy fonts
gulp.task('copy-assets', function(){
    return gulp.src(src.fonts)
        .pipe(gulp.dest(dist.fontsDir));
});

gulp.task('style-vendors', function() {
    return gulp.src([
        'src/assets/css/vendors/**/*.css'
    ])
        .pipe(concat('vendors.css'))
        .pipe(cssnano())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(dist.cssDir));
});

gulp.task('css', function() {
    return gulp.src([
        'src/assets/css/style.css',
        'src/assets/css/styles.css',
    ])
    .pipe(concat('style.css'))
    .pipe(cssnano({
        discardComments: {removeAll: true}
    }))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(dist.cssDir))
    .pipe(browserSync.stream());
});

gulp.task('script-vendors', function(done) {
    return gulp.src([
        'node_modules/popper.js/dist/umd/popper.js',
        'node_modules/bootstrap/js/dist/util.js',
        'node_modules/bootstrap/js/dist/collapse.js',
        'node_modules/bootstrap/js/dist/tab.js',
        'node_modules/owl.carousel/src/js/owl.carousel.js',
        'node_modules/owl.carousel/src/js/owl.navigation.js',
        'node_modules/scrollmagic/scrollmagic/minified/ScrollMagic.min.js',
        'node_modules/scrollmagic/scrollmagic/minified/plugins/debug.addIndicators.min.js',
        'node_modules/jquery-validation/dist/jquery.validate.js',
        'src/assets/js/vendors/**/*.js'
    ])
    .pipe(concat('vendors.js'))
    .pipe(uglify())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(dist.scriptsDir));
});

//Compile JS
gulp.task('scripts', function() {
    return gulp.src([
            src.assetsDir + 'js/main.js'
        ])
        .pipe(gulpif(!DEVELOPMENT, uglify())).on('error', function(e){
            console.log(e);
         })
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(dist.scriptsDir))
        .pipe(browserSync.stream());
});

//Optimize Images
gulp.task('images', function() {
    return gulp.src(src.images)
     .pipe(imagemin({
        interlaced: true,
        progressive: true,
        svgoPlugins: [{removeViewBox: false}]
      }))
      .pipe(gulp.dest(dist.imagesDir));
});

//Compile HTML Templates
gulp.task('templates', function() {
    return gulp.src([
            src.templates,
        ])
        .pipe(twig())
        .pipe(gulp.dest(dist.htmlDir))
        .pipe(browserSync.stream());
});

gulp.task('server', function() {
    browserSync.init({
        proxy: 'local.pentavirate',
        reloadOnRestart: true,
        reloadDelay: 100,
        open: false,
        notify: false,
        port: 8000
    });
});

// Watch files and run tasks
gulp.task('watch', function() {
    gulp.watch(src.css, ['css']);
    gulp.watch(src.images, ['images']);
    gulp.watch(src.scripts, ['scripts']);
    gulp.watch(src.templatesAndIncludes, ['templates']);
});

gulp.task('clean', function() {
    //return del.sync(dist.imagesDir);
});

gulp.task('vendors', function() {
    return gulp.start('assets-vendors', 'style-vendors', 'script-vendors');
});
$(document).ready(function() {
  if ( $(window).width() < 576 ) {
    startCarousel();
  } else {
    $('.owl-carousel').addClass('off');
  }
  
  if ($('.master-slider').length) {
    var $ms = $('.master-slider');
    var horizontalSl = $ms.data('dir') === 'h';
    $ms.each(function() {
      $(this).masterslider({
        width: 1852,    // slider standard width
        height: 819,   // slider standard height
        layout: 'fillwidth',
        dir: horizontalSl ? 'h' : 'v',
        view: 'parallaxMask',
        autoplay: true,
        speed: 25,
        space: 5,
        loop: true,
        minHeight: 280,
        overPause: false,
        controls: {
          bullets: { autohide: false, overVideo: true, dir: horizontalSl ? 'h' : 'v', align: horizontalSl ? 'bottom' : 'right', space: null, margin: 20  }
        }
      });
    })
  }

 /* var scrollController = new ScrollMagic.Controller();
  $(".container .row").each(function(i, elem) {
    var scrollM = new ScrollMagic.Scene({
      triggerElement: $(this).closest('.section')[0],
      triggerHook: .8, // show, when scrolled .8% into view
      duration: "180%", // hide 150%
      offset: 50, // move trigger to center of element
    })
    .setClassToggle(this, "visible") // add class to reveal
    // .addIndicators({name: "1 (duration: 0)"}) // debug option
    .addTo(scrollController);
  });*/
});

$(window).resize(function() {
  if ( $(window).width() < 576 ) {
    startCarousel();
  } else {
    stopCarousel();
  }
});

function startCarousel(){
  $("#team_slider").owlCarousel({
    navigation : true, // Show next and prev buttons
    slideSpeed : 500,
    center: true,
    margin:0,
    paginationSpeed : 400,
    autoplay:true,
    items : 1,
    loop: true,
    nav: false,
    dots: true
  });
}
function stopCarousel() {
  var owl = $('.owl-carousel');
  owl.trigger('destroy.owl.carousel');
  owl.addClass('off');
}

// Video player
$('.video .js-play').on('click',function(ev){
  $(this).parent().parent().find('.video-popup').addClass('is-open');
  
  var options = {
      loop: false,
      controls: true
  };
  
  var iframe = $(this).parent().parent().find('.videoPopupPlayer');
  var player = new Vimeo.Player(iframe, options);
  
  player.play().then(function() {
    //console.log("play");
    // the video was played
  }).catch(function(error) {
    switch (error.name) {
      case 'PasswordError':
      // the video is password-protected and the viewer needs to enter the
      // password first
      break;
      
      case 'PrivacyError':
      // the video is private
      break;
      
      default:
      // some other error occurred
      break;
    }
  });
  
  player.on('ended', function(data) {
    //console.log("ended", data.seconds);
    $('.video-popup').removeClass('is-open');
  });
  
  ev.preventDefault();
});

$('.video .js-close').on('touchstart click',function(ev){
  $(this).parent('.video-popup').removeClass('is-open');
  var iframe = $(this).parent().parent().find('.videoPopupPlayer');
  var player = new Vimeo.Player(iframe);
  
  player.pause().then(function() {
    // the video was paused
  }).catch(function(error) {
    switch (error.name) {
      case 'PasswordError':
      // the video is password-protected and the viewer needs to enter the
      // password first
      break;
      
      case 'PrivacyError':
      // the video is private
      break;
      
      default:
      // some other error occurred
      break;
    }
  });
  
  var iframe1 = $(this).parent().parent().find('.videoPlayer');
  var player1 = new Vimeo.Player(iframe1);
  
  player1.play().then(function() {
    // the video was played
  }).catch(function(error) {
    switch (error.name) {
      case 'PasswordError':
      // the video is password-protected and the viewer needs to enter the
      // password first
      break;
      
      case 'PrivacyError':
      // the video is private
      break;
      
      default:
      // some other error occurred
      break;
    }
  });
  
  ev.preventDefault();
});

$(window).on('load', function() {
  $('.tab-content .product__top__main').matchHeight();
});

$(".resources-nav-items .nav-link").on("click", function() {
  var cat = $(this).attr("id");
  $( ".resource-item" ).each(function( index ) {
    if( $(this).attr('data-cat') == cat ){
      $(this).show(0);
    }else{
      $(this).hide(0);
    }
  })
  if( cat == 'all-tab' ){
    $( ".resource-item" ).show(0);
   }
});

if ($('.nav-link[data-tab-link]').length) {
  $('.nav-link[data-tab-link]').on("click", function(el) {
    $('.tab-content[data-tab-anchor="' + $(this).data('tab-link') + '"]').children().each(function() {
      $(this).tab("show");
    });
  });
}

